var data = [{
        nombre: "Pólvora",
        unidad: "g",
        ingresos: [{
            cantidad: "1000",
            fechaCreacion: formatDate("2021-01-03")
        }, ]
    },
    {
        nombre: "Agua",
        unidad: "L",
        ingresos: [{
            cantidad: "2",
            fechaCreacion: formatDate("2021-03-31")
        }, ]
    },
    {
        nombre: "Gas",
        unidad: "L",
        cantidad: "10",
        ingresos: [{
            cantidad: "10",
            fechaCreacion: formatDate("2021-02-14")
        }]
    },
    {
        nombre: "Hojas (filo)",
        unidad: "Uni.",
        ingresos: []
    },
    {
        nombre: "Equipo maniobras",
        unidad: "Uni.",
        ingresos: []
    },
];

var icon = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg>';

function displaySelected() {
    changeContentDiv("inputSelected");
    addOptionsToSelect("recursosInicialesSeleccionados", document.getElementById("listadoRecursosIniciales").value);
}

function updateSelected(idSelect) {
    var index = document.getElementById(idSelect).value;
    displayUnidadMedida(data[index].unidad);
}

function displayTable() {
    var index = document.getElementById("recursosInicialesSeleccionados").value;
    if (!addElement(false))
        return;
    changeContentDiv("displayTable");
    addOptionsToSelect("recursosEnTabla", index);
    displayUnidadMedida(data[index].unidad);
    addContentTable();
}

function changeContentDiv(id) {
    var classes = document.getElementsByClassName("bodyCardFormat");
    for (var i = 0; i < classes.length; i++)
        classes[i].classList.add("hideDiv");
    document.getElementById(id).classList.remove("hideDiv");
}

function addOptionsToSelect(idSelect, index) {
    var select = document.getElementById(idSelect);
    data.forEach(function (element, i) {
        var elem = document.createElement('option');
        elem.textContent = element.nombre;
        elem.value = i;
        if (index != null && index == i) {
            elem.selected = true;
            displayUnidadMedida(element.unidad);
        }
        select.appendChild(elem);
    });
}

function displayUnidadMedida(value) {
    var append = document.getElementsByClassName("unidadMedida");
    for (var i = 0; i < append.length; i++)
        append[i].innerHTML = value;
}

function addContentTable() {
    var contenidoTabla = document.getElementById("tablaRecursosBody");
    data.forEach(function (element, i) {
        addMultiplesInputs(element, contenidoTabla, i);
    });
}

function addMultiplesInputs(data, contenidoTabla, index) {
    (data.ingresos).forEach(function (element, i) {
        if (element != null) {
            var tr = document.createElement('tr');
            tr.appendChild(createTD(data.nombre));
            tr.appendChild(createTD(element.cantidad + " " + data.unidad));
            tr.appendChild(createTD(element.fechaCreacion));
            tr.appendChild(createTD('<a href="#" onclick="deleteRow(' + "Elemento" + index + 'Numero' + i + ')">' + icon + '</a>'));
            tr.id = "Elemento" + index + 'Numero' + i;
            contenidoTabla.appendChild(tr);
        }
    });
}

function createTD(element) {
    var td = document.createElement('td');
    td.innerHTML = element;
    return td;
}

function deleteRow(selected) {
    var element = (selected.id).split('Elemento')[1];
    var pos = element.split('Numero')[1];
    element = element.split('Numero')[0];
    data[element].ingresos[pos] = null;
    selected.remove();
}

function addElement(isTableShowed) {
    var value = parseFloat(document.getElementById("inputBeforeTable").value);
    if (value <= 0 || isNaN(value) || !Number.isInteger(value)) {
        alert("El valor ingresado debe ser numérico, entero y mayor a 0.");
        return false;
    }
    var selected = document.getElementById("recursosInicialesSeleccionados").value;
    var fechaCreacion = new Date().toLocaleDateString("es-CL");

    if (isTableShowed) {
        selected = document.getElementById("recursosEnTabla").value;
        value = parseFloat(document.getElementById("inputAfterTable").value);
        if (value <= 0 || isNaN(value) || !Number.isInteger(value)) {
            alert("El valor ingresado debe ser numérico, entero y mayor a 0.");
            return false;
        }

        var contenidoTabla = document.getElementById("tablaRecursosBody");
        var tr = document.createElement('tr');
        tr.appendChild(createTD(data[selected].nombre));
        tr.appendChild(createTD(value + " " + data[selected].unidad));
        tr.appendChild(createTD(fechaCreacion));
        tr.appendChild(createTD('<a href="#" onclick="deleteRow(' + "Elemento" + selected + 'Numero' + data[selected].ingresos.length + ')">' + icon + '</a>'));
        tr.id = "Elemento" + selected + 'Numero' + data[selected].ingresos.length;
        contenidoTabla.appendChild(tr);
    }
    data[selected].ingresos.push({
        cantidad: value,
        fechaCreacion: fechaCreacion
    });
    return true;
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

$(document).ready(function () {
    addOptionsToSelect("listadoRecursosIniciales", null);
});