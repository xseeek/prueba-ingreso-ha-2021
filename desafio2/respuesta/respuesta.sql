-- 1.-
SELECT titanes.nombre, titanes.altura, muertes.fecha FROM titanes
INNER JOIN muertes ON titanes.id = muertes.id_titan
WHERE muertes.causa LIKE 'Accidente'
ORDER BY muertes.fecha ASC;

-- 2.-
SELECT titanes.nombre, titanes.altura FROM titanes
INNER JOIN muertes ON titanes.id = muertes.id_titan
WHERE muertes.causa LIKE 'Batallón 1'
ORDER BY titanes.altura DESC
LIMIT 1;

-- 3.-
SELECT titanes.nombre, titanes.altura FROM prueba_healthatom_2.avistamientos
INNER JOIN titanes ON titanes.id = id_titan
LEFT JOIN muertes ON titanes.id = muertes.id_titan
WHERE muertes.id_titan IS NULL
GROUP BY avistamientos.id_titan
HAVING MAX(avistamientos.fecha);

-- 4.-
SELECT titanes.nombre FROM titanes
INNER JOIN avistamientos ON titanes.id = avistamientos.id_titan
GROUP BY titanes.id, YEAR(fecha)
HAVING (COUNT(YEAR(fecha)) > 1)
ORDER BY YEAR(fecha) ASC;

-- 5.-
SELECT recursos.nombre, SUM(movimientos_recursos.cantidad) as 'cantidad', recursos.unidad FROM recursos
INNER JOIN movimientos_recursos ON recursos.id = movimientos_recursos.id_recurso
INNER JOIN muertes ON muertes.id = movimientos_recursos.id_muerte
INNER JOIN titanes ON titanes.id = muertes.id_titan
WHERE titanes.altura <= 5 AND movimientos_recursos.id_movimiento = 2
GROUP BY recursos.id;

-- 6.-
SELECT recursos.nombre FROM recursos
INNER JOIN movimientos_recursos ON recursos.id = movimientos_recursos.id_recurso
INNER JOIN muertes ON muertes.id = movimientos_recursos.id_muerte
INNER JOIN titanes ON titanes.id = muertes.id_titan
WHERE titanes.altura = 9
GROUP BY recursos.id
ORDER BY COUNT(*) DESC
LIMIT 1;

-- 7.-
SELECT titanes.nombre FROM titanes
INNER JOIN muertes on titanes.id = muertes.id_titan
INNER JOIN avistamientos ON titanes.id = avistamientos.id_titan
WHERE avistamientos.fecha > muertes.fecha;

-- 8.-
-- Para poder determinar si un titan ha muerto o no se debe confirmar en el momento la causa de muerte. Los titanes que presentan incongruencias en
-- avistamientos posteriores a su muerte pueden estar relacionados al hecho que fueron heridos de gravedad pero pudieron escapar y superar el ataque.
-- Esto puede llevar a que se asuman muertes que nunca fueron concretadas y confirmadas.