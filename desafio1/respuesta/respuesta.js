/**
 * Se carga el arreglo de datos, donde se encuentran almacenados las parejas de dias.
 */
const data = require("../last_year.json");

const menosTitansAvistados = (dias) => {
    /**
     * Se define el mínimo(primer dia de febrero) y el máximo(ultimo dia de noviembre) para obtener la solución. Mas adelante, se entiende como 
     * "ocurrencia" a los avistamientos de titanes fuera de las murallas.
     */
    const min = 31;
    const max = 333;
    let menorAvistamientos = {
        "dia": min,
        "ocurrencias": 0
    };

    /**
     * Iteramos a partir del rango de dias definido previamente.
     */
    for (let i = min; i <= max; i++) {
        /**
         * Ocupamos filter para obtener la cantidad de elementos que están dentro de los dias iterados (i). Al obtener el largo del arreglo
         * obtenemos la cantidad de ocurrencias, las que se pueden interpretar como avistamientos, para obtener el dia con sus avistamientos.
         */
        let ocurrencias = dias.filter(
            (avistamiento) => {
                if (avistamiento[0] >= 364)
                    avistamiento[0] -= 364;
                if (avistamiento[1] >= 364)
                    avistamiento[1] -= 364;
                return (i >= avistamiento[0] && i <= avistamiento[1]);
            }).length;
        /**
         * En el caso que el dia tenga 0 ocurrencias, es el mejor caso posible, por lo que lo almacenamos en la variable que ocupamos para la
         * solución y terminamos la iteración de los dias.
         */
        if (ocurrencias == 0) {
            menorAvistamientos = {
                dia: i,
                ocurrencias: ocurrencias
            };
            break;
        }
        /**
        * Si nos encontramos en la primera iteración, inicializamos con el primer dia y ocurrencias
        */
        if (menorAvistamientos.ocurrencias == 0)
            menorAvistamientos = {
                dia: i,
                ocurrencias: ocurrencias
            };
        /**
         * En el caso contrario, si logramos encontrar una ocurrencia menor a la que se tiene actualmente, se actualiza el dia y las ocurrencias
         * para poder obtener la solución.
         */
        if (menorAvistamientos.ocurrencias > ocurrencias)
            menorAvistamientos = {
                dia: i,
                ocurrencias: ocurrencias
            };
        /**
         * Dado que el arreglo de dias no tiene orden especifico, puede existir un caso en el que el dia definido como mejor solución sea mayor al
         * obtenido. Por lo que debemos actualizarlo por este dia (Según se solicita que se retorne el primer dia del año en poseer menos ocurrencias).
         */
        else if (menorAvistamientos.ocurrencias == ocurrencias && menorAvistamientos.dia > (i + 1))
            menorAvistamientos = {
                dia: i,
                ocurrencias: ocurrencias
            };
    }
    /**
     * Retornamos el dia con la menor cantidad de ocurrencias.
     */
    return (menorAvistamientos.dia + 1);
};

console.log("Dia con menos avistamientos: " + menosTitansAvistados(data));