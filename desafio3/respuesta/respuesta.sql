-- Primero se crea la tabla de responsables
CREATE TABLE IF NOT EXISTS responsables (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255)
);

-- Se agregan las columnas hora y ubicacion con el fin de tener un detalle mas especifico en cada avistamiento, ademas de incluir
-- quien fue el responsable de reportar dicho avistamiento.
ALTER TABLE avistamientos
ADD COLUMN hora TIME DEFAULT NULL AFTER fecha,
ADD COLUMN ubicacion VARCHAR(20) DEFAULT 'No reportada' AFTER hora,
ADD COLUMN id_responsable INT DEFAULT NULL AFTER id_titan,
ADD FOREIGN KEY responsable_avistamiento(id_responsable) REFERENCES responsables(id) ON DELETE SET NULL;

-- Tal como en la tabla de avistamientos, se registra un responsable por cada movimiento(sea entrada o salida) para tener registro de estos.
ALTER TABLE movimientos_recursos
ADD COLUMN id_responsable INT DEFAULT NULL AFTER id_muerte,
ADD FOREIGN KEY responsable_movimiento(id_responsable) REFERENCES responsables(id) ON DELETE SET NULL;

-- Se agrega un responsable a las muertes, esto debido a que no se puede asumir que el responsable del movimiento del recurso es el mismo responsable
-- que autorizo la ejecucion del titan. Puede haberse autorizado un movimiento pero no haber terminado en una muerte.
ALTER TABLE muertes
ADD COLUMN id_responsable INT DEFAULT NULL AFTER id_titan,
ADD FOREIGN KEY responsable_muerte(id_responsable) REFERENCES responsables(id) ON DELETE SET NULL;

INSERT INTO responsables(nombre) VALUES ('Sujeto 1');
INSERT INTO responsables(nombre) VALUES ('Sujeto 2');

UPDATE `muertes` SET `id_responsable` = '2' WHERE (`id` = '7');
UPDATE `muertes` SET `id_responsable` = '2' WHERE (`id` = '13');

SELECT responsables.nombre, COUNT(*) as 'Muertes' FROM responsables
INNER JOIN muertes ON muertes.id_responsable = responsables.id
WHERE muertes.causa NOT LIKE 'Accidente' AND (YEAR(fecha)) = 2020
GROUP BY responsables.id, (YEAR(fecha))
ORDER BY COUNT(*) DESC
LIMIT 1;
